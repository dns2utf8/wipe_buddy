
liveImage: clean release iso/archlinux-2017.02.01-dual.iso
	mkdir -p target/customiso target/archiso
	sudo mount iso/archlinux-2017.02.01-dual.iso target/archiso
	cp -a target/archiso target/customiso
	cd target/customiso/archiso/arch/x86_64/ && sudo unsquashfs airootfs.sfs
	sudo cp target/release/whipe_buddy target/customiso/archiso/arch/x86_64/squashfs-root/root/
	
	cd target/customiso/archiso/arch/x86_64/ && rm -f airootfs.sfs && sudo mksquashfs squashfs-root airootfs.sfs
	cd target/customiso/archiso/arch/x86_64/ && sudo rm -rf squashfs-root && md5sum airootfs.sfs > airootfs.md5
	
	cd target/customiso/archiso && genisoimage -l -r -J -V "ARCH_201702" -b isolinux/isolinux.bin -no-emul-boot -boot-load-size 4 -boot-info-table -c isolinux/boot.cat -o ../arch-custom.iso ./


clean:
	sudo umount target/archiso || true
	sudo rm -rf target/customiso
	cargo clean
	
release:
	cargo build --release
	
installDeps:
	sudo pacman -S squashfs-tools cdrtools
