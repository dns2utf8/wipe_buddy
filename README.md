# Project wipe_buddy

[![build status](https://gitlab.com/dns2utf8/wipe_buddy/badges/master/pipeline.svg)](https://gitlab.com/dns2utf8/wipe_buddy/)

Wipe thinkpad laptops with ease.

## Features

  - [x] Detect all physical disks
  - [x] Wipe all disks in parallel
  - [x] Adjust backlight brightness to max
  - [x] Allow direct shutdown from application
  - [ ] Protect physical disk mounted via cryptsetup or lvm
  - [ ] Queue shutdown until all selected disks are wiped
  - [ ] Rescan disks without application restart
  - [ ] Detect hotplugged disks
  - [ ] Show progressbar

## System requirements

I developed this tool with [Arch Linux](https://www.archlinux.org/) as such it relies upon that platfrom.
However, it should run on other linux platfroms as well.

It works on the latest stable rust, currently 1.17.0.

## Setup instructions

To use this project I recommend installing a minimal Arch Linux on a usb stick (4GB or more).

Compile wipe_buddy with `cargo build --release`.
The resulting binary should be placed inside the home directory of root or the sticks `/usr/bin/`.

Then run it as `root` or with `sudo wipe_buddy`.

Note: The stick does not need to connect to the network or run a graphical user interface.

### TODO autostart wipe_buddy

  - https://wiki.archlinux.org/index.php/Getty
  - https://bbs.archlinux.org/viewtopic.php?id=129509

# Author

Stefan Schindler [@dns2utf8](https://twitter.com/dns2utf8)
